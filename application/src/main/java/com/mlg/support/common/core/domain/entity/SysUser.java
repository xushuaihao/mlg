package com.mlg.support.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mlg.support.common.annotation.Excel;
import com.mlg.support.common.core.domain.BaseEntity;
import com.mlg.support.common.xss.Xss;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
public class SysUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Excel(name = "用户Id", cellType = Excel.ColumnType.NUMERIC, prompt = "用户Id")
    @TableField("user_id")
    private Long userId;

    /**
     * 用户账号
     */
    @Excel(name = "用户名")
    @TableField("user_name")
    private String userName;

    /**
     * 用户姓名
     */
    @Excel(name = "真实姓名")
    @TableField("real_name")
    private String realName;

    /**
     * 密码
     */
    @Excel(name = "密码")
    @TableField("password")
    private String password;

    /**
     * 用户性别
     */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    @TableField("sex")
    private String sex;

    /**
     * 用户生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户生日", dateFormat = "yyyy-MM-dd")
    @TableField("birthday")
    private Date birthday;

    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    @TableField("phone")
    private String phone;

    /**
     * 用户邮箱
     */
    @Excel(name = "用户邮箱")
    @TableField("email")
    private String email;

    /**
     * 门店Id
     */
    @Excel(name = "门店Id")
    @TableField("gstore_id")
    private Long gstoreId;

    /**
     * 门店编码
     */
    @Excel(name = "门店编码")
    private String gstoreCode;

    /**
     * 底薪（销售员用）
     */
    @Excel(name = "底薪（销售员用）")
    @TableField("basic_salary")
    private String basicSalary;

    /**
     * 每单提成（销售员用）
     */
    @Excel(name = "每单提成（销售员用）")
    @TableField("commission")
    private String commission;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    @TableField("status")
    private String status;

    /**
     * 最后登录IP
     */
    @Excel(name = "最后登录IP", type = Excel.Type.EXPORT)
    @TableField("login_ip")
    private String loginIp;

    /**
     * 最后登录时间
     */
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    @TableField("login_date")
    private Date loginDate;

    /**
     * 角色对象
     */
    private List<SysRole> roles;

    /**
     * 角色组
     */
    private Long[] roleIds;

    /**
     * 角色ID
     */
    private Long roleId;

    public SysUser() {
    }

    public SysUser(Long userId) {
        this.userId = userId;
    }

    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

    public boolean isAdmin() {
        return isAdmin(this.userId);
    }

    @Xss(message = "用户姓名不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户姓名长度不能超过30个字符")
    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Long getGstoreId() {
        return gstoreId;
    }

    public void setGstoreId(Long gstoreId) {
        this.gstoreId = gstoreId;
    }

    public String getGstoreCode() {
        return gstoreCode;
    }

    public void setGstoreCode(String gstoreCode) {
        this.gstoreCode = gstoreCode;
    }

    public String getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(String basicSalary) {
        this.basicSalary = basicSalary;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public List<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public Long[] getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds) {
        this.roleIds = roleIds;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("userId", getUserId())
                .append("userName", getUserName())
                .append("realName", getRealName())
                .append("password", getPassword())
                .append("sex", getSex())
                .append("birthday", getBirthday())
                .append("email", getEmail())
                .append("phone", getPhone())
                .append("gstoreId", getGstoreId())
                .append("gstoreCode", getGstoreCode())
                .append("basicSalary", getBasicSalary())
                .append("commission", getCommission())
                .append("status", getStatus())
                .append("isDelete", getIsDelete())
                .append("loginIp", getLoginIp())
                .append("loginDate", getLoginDate())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
