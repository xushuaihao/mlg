package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.mapper.GlassesIndentDetailsMapper;
import com.mlg.support.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesInventoryMapper;
import com.mlg.module.glasses.domain.GlassesInventory;
import com.mlg.module.glasses.service.IGlassesInventoryService;

import java.util.List;

/**
 * 门店库存管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesInventoryServiceImpl extends ServiceImpl<GlassesInventoryMapper, GlassesInventory> implements IGlassesInventoryService {

    @Autowired
    private GlassesIndentDetailsMapper indentDetailsMapper;

    @Override
    public List<GlassesInventory> selectInventoryList(GlassesInventory glassesInventory) {
        return getBaseMapper().selectInventoryList(glassesInventory);
    }

    @Override
    public GlassesInventory getInventoryById(Long id) {
        return getBaseMapper().selectInventoryById(id);
    }

    @Override
    public Boolean updateInventoryNumber(Long[] gdetailsIds) {
        int count = 0;
        for (Long gdetailsId : gdetailsIds) {
            GlassesIndentDetails details = indentDetailsMapper.selectById(gdetailsId);
            GlassesInventory inventory = new GlassesInventory();
            inventory.setGinventoryId(details.getGinventoryId());
            inventory.setNumber(details.getNumber());
            if (getBaseMapper().updateInventoryNumber(inventory)) {
                count += 1;
            }
        }
        return gdetailsIds.length == count;
    }

    @Override
    public int updateByInventoryId(GlassesInventory glassesInventory) {
        return getBaseMapper().updateByInventoryId(glassesInventory);
    }

    @Override
    public String checkInventoryUnique(GlassesInventory glassesInventory) {
        if (StringUtils.isNotNull(getBaseMapper().checkInventoryUnique(glassesInventory))){
            return "1";
        }
        return "0";
    }

    @Override
    public boolean addInventoryByGgoodsIdStoreId(GlassesInventory glassesInventory) {
        return getBaseMapper().addInventoryByGgoodsIdStoreId(glassesInventory);
    }

}