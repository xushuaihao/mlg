package com.mlg.module.glasses.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import com.mlg.support.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;


/**
 * 商品信息管理对象 glasses_goods
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_goods")
@ApiModel(value = "GlassesGoods对象", description = "商品信息管理")
public class GlassesGoods {

    private static final long serialVersionUID = 1L;

    /**
     * 商品Id
     */
    @ApiModelProperty("商品Id")
    @TableId(value = "ggoods_id", type = IdType.AUTO)
    private Integer ggoodsId;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    @ApiModelProperty("商品名称")
    @TableField("ggoods_name")
    private String ggoodsName;

    /**
     * 商品类型
     */
    @Excel(name = "商品类型")
    @ApiModelProperty("商品类型")
    @TableField("ggoods_type")
    private String ggoodsType;

    /**
     * 镜片度数
     */
    @Excel(name = "镜片度数")
    @ApiModelProperty("镜片度数")
    @TableField("lens_degree")
    private String lensDegree;

    /**
     * 商品价格
     */
    @Excel(name = "商品价格")
    @ApiModelProperty("商品价格")
    @TableField("ggoods_price")
    private BigDecimal ggoodsPrice;

    /**
     * 品牌Id
     */
    @Excel(name = "品牌Id")
    @ApiModelProperty("品牌Id")
    @TableField("gbrand_id")
    private Integer gbrandId;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String gbrandName;

    /**
     * 商品状态（0正常 1停用）
     */
    @Excel(name = "商品状态", readConverterExp = "0=正常,1=停用")
    @ApiModelProperty("商品状态（0正常 1停用）")
    @TableField("status")
    private String status;

    /**
     * 删除标志
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @ApiModelProperty("删除标志（0代表存在 1代表删除）")
    @TableField("is_delete")
    private Integer isDelete;

}
