package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;


/**
 * 品牌类型对象 glasses_brand
 *
 * @author xshao
 * @date 2023-08-18
 */
@Data
@Accessors(chain = true)
@TableName("glasses_brand")
@ApiModel(value = "GlassesBrand对象", description = "品牌类型")
public class GlassesBrand {

    private static final long serialVersionUID = 1L;

    /**
     * 眼镜品牌Id
     */
    @ApiModelProperty("眼镜品牌Id")
    @TableId(value = "gbrand_id", type = IdType.AUTO)
    private Integer gbrandId;

    /**
     * 眼镜品牌名称
     */
    @Excel(name = "眼镜品牌名称")
    @ApiModelProperty("眼镜品牌名称")
    @TableField("gbrand_name")
    private String gbrandName;

    /**
     * 适应人群
     */
    @Excel(name = "适应人群")
    @ApiModelProperty("适应人群")
    @TableField("adapt_crowd")
    private String adaptCrowd;

    /**
     * 品牌状态（0正常 1停用）
     */
    @Excel(name = "品牌状态", readConverterExp = "0=正常,1=停用")
    @ApiModelProperty("品牌状态（0正常 1停用）")
    @TableField("status")
    private String status;

    /**
     * 删除标志
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @ApiModelProperty("删除标志（0代表存在 1代表删除）")
    @TableField("is_delete")
    private Integer isDelete;

}
