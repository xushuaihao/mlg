package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesInventory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 门店库存管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesInventoryMapper extends BaseMapper<GlassesInventory> {

    public List<GlassesInventory> selectInventoryList(@Param("glassesInventory") GlassesInventory glassesInventory);

    public GlassesInventory selectInventoryById(Long id);

    public boolean updateInventoryNumber(@Param("glassesInventory") GlassesInventory glassesInventory);

    public int updateByInventoryId(GlassesInventory glassesInventory);


    /**
     * 根据库存Id，申请数量新增门店商品库存
     *
     * @param inventory
     * @return
     */
    public boolean addInventoryByStore(@Param("inventory") GlassesInventory inventory);

    /**
     * 根据商品Id、门店添加库存
     *
     * @param inventory
     * @return
     */
    public boolean addInventoryByGgoodsIdStoreId(@Param("inventory") GlassesInventory inventory);

    /**
     * 根据商品Id，申请数量从总公司库存中减去相应数量
     *
     * @param inventory
     * @return
     */
    public boolean minusInventoryByStore(@Param("inventory") GlassesInventory inventory);

    /**
     * 通过门店和商品查库存
     *
     * @param inventory
     * @return
     */
    public GlassesInventory selectInventoryByStoreAndGoods(@Param("inventory") GlassesInventory inventory);

    /**
     * 检查库存唯一性
     *
     * @param inventory
     * @return
     */
    public GlassesInventory checkInventoryUnique(@Param("inventory") GlassesInventory inventory);
}
