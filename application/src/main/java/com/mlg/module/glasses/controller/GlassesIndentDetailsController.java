package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mlg.module.glasses.domain.GlassesInventory;
import com.mlg.module.glasses.domain.GlassesStore;
import com.mlg.module.glasses.service.IGlassesInventoryService;
import com.mlg.support.common.core.domain.R;
import com.mlg.support.common.core.domain.Ret;
import com.mlg.support.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.service.IGlassesIndentDetailsService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 订单详情管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "订单详情管理模块")
@RestController
@RequestMapping("/glasses/details")
public class GlassesIndentDetailsController extends BaseController {

    @Autowired
    private IGlassesIndentDetailsService glassesIndentDetailsService;

    @Autowired
    private IGlassesInventoryService glassesInventoryService;

    /**
     * 查询订单详情管理列表
     */
    @ApiOperation(value = "查询订单详情管理列表")
//    @PreAuthorize("@ss.hasPermi('glasses:details:query')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesIndentDetails details) {
        startPage();
        List<GlassesIndentDetails> detailsList = glassesIndentDetailsService.selectDetailsList(details);
        return getDataTable(detailsList);

    }

    /**
     * 导出订单详情管理列表
     */
    @ApiOperation(value = "导出订单详情管理列表")
    @PreAuthorize("@ss.hasPermi('glasses:details:export')")
    @Log(title = "订单详情管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesIndentDetails glassesIndentDetails) {
        LambdaQueryWrapper<GlassesIndentDetails> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesIndentDetails> list = glassesIndentDetailsService.list(queryWrapper);
        ExcelUtil<GlassesIndentDetails> util = new ExcelUtil<GlassesIndentDetails>(GlassesIndentDetails.class);
        util.exportExcel(response, list, "订单详情管理数据");
    }


    /**
     * 获取订单详情管理详细信息
     */
    @ApiOperation(value = "获取订单详情管理详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:details:query')")
    @GetMapping(value = "/{gindentId}")
    public AjaxResult getInfo(@PathVariable("gindentId") Long id) {
        return success(glassesIndentDetailsService.getById(id));
    }

    /**
     * 新增订单详情管理
     */
    @ApiOperation(value = "新增订单详情管理")
    @PreAuthorize("@ss.hasPermi('glasses:details:add')")
    @Log(title = "订单详情管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesIndentDetails glassesIndentDetails) {
        return toAjax(glassesIndentDetailsService.save(glassesIndentDetails));
    }

    /**
     * 修改订单详情管理
     */
    @ApiOperation(value = "修改订单详情管理")
    @PreAuthorize("@ss.hasPermi('glasses:details:edit')")
    @Log(title = "订单详情管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesIndentDetails glassesIndentDetails) {
        return toAjax(glassesIndentDetailsService.updateById(glassesIndentDetails));
    }


    /**
     * 删除订单详情管理
     */
    @ApiOperation(value = "删除订单详情管理")
    @PreAuthorize("@ss.hasPermi('glasses:details:remove')")
    @Log(title = "订单详情管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{gdetailsIds}")
    public AjaxResult remove(@PathVariable Long[] gdetailsIds) {
        Boolean success1 = glassesInventoryService.updateInventoryNumber(gdetailsIds);
        Boolean success2 = glassesIndentDetailsService.removeDetailsByIds(gdetailsIds);
        return toAjax(success1&&success2);
    }


    /**
     * 批量添加商品给订单
     */
    @PreAuthorize("@ss.hasPermi('glasses:details:edit')")
    @Log(title = "订单详情管理", businessType = BusinessType.GRANT)
    @PostMapping("/addGoodsToDetails")
    public AjaxResult addGoodsToDetails(@RequestBody List<GlassesIndentDetails> detailsList) {
        return toAjax(glassesIndentDetailsService.addGoodsToDetails(detailsList));
    }

}
