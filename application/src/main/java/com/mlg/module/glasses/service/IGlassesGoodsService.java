package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品信息管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesGoodsService extends IService<GlassesGoods> {

    /**
     * 查询商品信息列表
     *
     * @param glassesGoods
     * @return
     */
    public List<GlassesGoods> selectGoodsList(@Param("glassesGoods") GlassesGoods glassesGoods);

    /**
     * 通过商品Id获取商品信息
     *
     * @param id
     * @return
     */
    public GlassesGoods getGoodsById(Long id);
}