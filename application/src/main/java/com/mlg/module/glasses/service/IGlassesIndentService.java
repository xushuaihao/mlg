package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesIndent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单信息管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesIndentService extends IService<GlassesIndent> {

    public List<GlassesIndent> selectIndentList(@Param("glassesIndent") GlassesIndent glassesIndent);

    public GlassesIndent getIndentById(Long id);

    public int updateIndent(@Param("glassesIndent") GlassesIndent glassesIndent);

    public boolean removeIndentById(Long gindentId);
}