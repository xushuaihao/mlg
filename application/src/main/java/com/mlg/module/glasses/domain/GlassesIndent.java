package com.mlg.module.glasses.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mlg.support.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;


/**
 * 订单信息管理对象 glasses_indent
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_indent")
@ApiModel(value = "GlassesIndent对象", description = "订单信息管理")
public class GlassesIndent extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 订单Id
     */
    @ApiModelProperty("订单Id")
    @TableId(value = "gindent_id", type = IdType.AUTO)
    private Long gindentId;

    /**
     * 客户Id
     */
    @Excel(name = "客户Id")
    @ApiModelProperty("客户Id")
    @TableField("customer_id")
    private Long customerId;

    /**
     * 客户姓名
     */
    @Excel(name = "客户姓名")
    private String customerName;

    /**
     * 客户电话
     */
    @Excel(name = "客户电话")
    private String phone;

    /**
     * 取件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("取件时间")
    @TableField("pickup_time")
    private Date pickupTime;

    /**
     * 门店Id
     */
    @Excel(name = "门店Id")
    @ApiModelProperty("门店Id")
    @TableField("gstore_id")
    private Long gstoreId;

    /**
     * 眼镜店编号
     */
    @Excel(name = "眼镜店编号")
    private String gstoreCode;

    /**
     * 销售员
     */
    @Excel(name = "销售员")
    @ApiModelProperty("销售员")
    @TableField("user_id")
    private Long userId;

    /**
     * 销售员
     */
    @Excel(name = "销售员姓名")
    private String realName;

    /**
     * 订单金额，总计
     */
    @Excel(name = "订单金额，总计")
    @ApiModelProperty("订单金额，总计")
    @TableField("price")
    private BigDecimal price;

    /**
     * 客户评价（0差，1良，2优）
     */
    @Excel(name = "客户评价", readConverterExp = "0=差，1良，2优")
    @ApiModelProperty("客户评价（0差，1良，2优）")
    @TableField("evaluate")
    private String evaluate;

    /**
     * 改进建议
     */
    @Excel(name = "改进建议")
    @ApiModelProperty("改进建议")
    @TableField("improvement")
    private String improvement;

    /**
     * 状态（0：待取件，1：待评价，2：已完成）
     */
    @Excel(name = "状态", readConverterExp = "0=：待取件，1：待评价，2：已完成")
    @ApiModelProperty("状态（0：待取件，1：待评价，2：已完成）")
    @TableField("status")
    private String status;


}
