package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户信息管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesCustomerMapper extends BaseMapper<GlassesCustomer> {

    public List<GlassesCustomer> selectPageList(@Param("glassesCustomer") GlassesCustomer glassesCustomer);
}
