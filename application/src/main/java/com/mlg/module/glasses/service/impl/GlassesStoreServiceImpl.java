package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mlg.support.common.constant.UserConstants;
import com.mlg.support.common.core.domain.entity.SysUser;
import com.mlg.support.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesStoreMapper;
import com.mlg.module.glasses.domain.GlassesStore;
import com.mlg.module.glasses.service.IGlassesStoreService;

import java.util.List;
/**
 * 门店管理Service业务层处理
 *
 * @author xshao
 * @date 2023-08-23
 */
@Service
public class GlassesStoreServiceImpl extends ServiceImpl<GlassesStoreMapper, GlassesStore> implements IGlassesStoreService {

    @Autowired
    private GlassesStoreMapper storeMapper;

    @Override
    public List<GlassesStore> selectStoreList(GlassesStore glassesStore) {
        return getBaseMapper().selectPageList(glassesStore);
    }

    @Override
    public GlassesStore getStoreById(Long id) {
        return getBaseMapper().selectStoreById(id);
    }

    @Override
    public int updateStoreStatus(GlassesStore store) {
        return storeMapper.updateGstoreById(store);
    }

    @Override
    public String checkStoreCodeUnique(GlassesStore glassesStore) {
        Long gStoreId = StringUtils.isNull(glassesStore.getGstoreId()) ? -1L : glassesStore.getGstoreId();
        GlassesStore info = storeMapper.checkStoreCodeUnique(glassesStore.getGstoreCode());
        if (StringUtils.isNotNull(info) && info.getGstoreId().longValue() != gStoreId.longValue()) {
            return "1";
        }
        return "0";
    }

    @Override
    public String checkUserIdUnique(GlassesStore glassesStore) {
        Long gStoreId = StringUtils.isNull(glassesStore.getGstoreId()) ? -1L : glassesStore.getGstoreId();
        GlassesStore info = storeMapper.checkUserIdUnique(glassesStore.getUserId());
        if (StringUtils.isNotNull(info) && info.getGstoreId().longValue() != gStoreId.longValue()) {
            return "1";
        }
        return "0";
    }

    @Override
    public int updateGstoreById(GlassesStore glassesStore) {
        return storeMapper.updateGstoreById(glassesStore);
    }
}