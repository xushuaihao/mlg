package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mlg.support.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;


/**
 * 客户信息管理对象 glasses_customer
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_customer")
@ApiModel(value = "GlassesCustomer对象", description = "客户信息管理")
public class GlassesCustomer {

    private static final long serialVersionUID = 1L;

    /**
     * 客户Id
     */
    @ApiModelProperty("客户Id")
    @TableId(value = "customer_id", type = IdType.AUTO)
    private Long customerId;

    /**
     * 客户姓名
     */
    @Excel(name = "客户姓名")
    @ApiModelProperty("客户姓名")
    @TableField("customer_name")
    private String customerName;

    /**
     * 性别（0，男；1，女；2，未知）
     */
    @Excel(name = "性别", readConverterExp = "0=，男；1，女；2，未知")
    @ApiModelProperty("性别（0，男；1，女；2，未知）")
    @TableField("sex")
    private String sex;

    /**
     * 年龄
     */
    @Excel(name = "年龄")
    @ApiModelProperty("年龄")
    @TableField("age")
    private Integer age;

    /**
     * 电话号码
     */
    @Excel(name = "电话号码")
    @ApiModelProperty("电话号码")
    @TableField("phone")
    private String phone;

    /**
     * 逻辑删除
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @JsonIgnore //发送转json时忽略
    @TableField("is_delete")
    private Integer isDelete;

}
