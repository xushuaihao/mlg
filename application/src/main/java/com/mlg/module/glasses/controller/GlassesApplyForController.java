package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mlg.module.glasses.domain.GlassesApplyForDetails;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.service.IGlassesInventoryService;
import com.mlg.support.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesApplyFor;
import com.mlg.module.glasses.service.IGlassesApplyForService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 库存申请信息管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "库存申请信息管理模块")
@RestController
@RequestMapping("/glasses/applyFor")
public class GlassesApplyForController extends BaseController {

    @Autowired
    private IGlassesApplyForService glassesApplyForService;
    @Autowired
    private IGlassesInventoryService glassesInventoryService;

    /**
     * 查询库存申请信息列表
     */
    @ApiOperation(value = "查询库存申请信息列表")
//    @PreAuthorize("@ss.hasPermi('glasses:applyFor:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesApplyFor applyFor) {
        startPage();
        List<GlassesApplyFor> applyFors = glassesApplyForService.selectApplyForList(applyFor);
        return getDataTable(applyFors);
    }

    /**
     * 导出库存申请信息列表
     */
    @ApiOperation(value = "导出库存申请信息列表")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:export')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesApplyFor glassesApplyFor) {
        LambdaQueryWrapper<GlassesApplyFor> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesApplyFor> list = glassesApplyForService.list(queryWrapper);
        ExcelUtil<GlassesApplyFor> util = new ExcelUtil<GlassesApplyFor>(GlassesApplyFor.class);
        util.exportExcel(response, list, "库存申请信息数据");
    }


    /**
     * 获取库存申请信息详细信息
     */
    @ApiOperation(value = "获取库存申请信息详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:query')")
    @GetMapping(value = "/{applyforId}")
    public AjaxResult getInfo(@PathVariable("applyforId") Long id) {
        return success(glassesApplyForService.getApplyForById(id));
    }

    /**
     * 新增库存申请信息
     */
    @ApiOperation(value = "新增库存申请信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:add')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesApplyFor glassesApplyFor) {
        return toAjax(glassesApplyForService.save(glassesApplyFor));
    }

    /**
     * 修改库存申请信息
     */
    @ApiOperation(value = "修改库存申请信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:edit')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
        public AjaxResult edit(@RequestBody GlassesApplyFor glassesApplyFor) {
        return toAjax(glassesApplyForService.updateById(glassesApplyFor));
    }

    /**
     * 删除库存申请信息
     */
    @ApiOperation(value = "删除库存申请信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:remove')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{applyforId}")
    public AjaxResult remove(@PathVariable Long applyforId) {
        return toAjax(glassesApplyForService.removeApplyForById(applyforId));
    }


    /**
     * 查询库存申请详情信息列表
     */
    @ApiOperation(value = "查询库存申请详情信息列表")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:list')")
    @GetMapping("/detailsList")
    public TableDataInfo detailsList(GlassesApplyForDetails applyForDetails) {
        startPage();
        List<GlassesApplyForDetails> applyForDetailsList = glassesApplyForService.selectApplyForDetailsList(applyForDetails);
        return getDataTable(applyForDetailsList);
    }

    /**
     * 批量添加商品给库存申请
     */
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:edit')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.GRANT)
    @PostMapping("/addGoodsToDetails")
    public AjaxResult addGoodsToDetails(@RequestBody List<GlassesApplyForDetails> applyForDetailsList) {
        return toAjax(glassesApplyForService.addGoodsToDetails(applyForDetailsList));
    }

    /**
     * 删除库存申请详情信息
     */
    @ApiOperation(value = "删除库存申请详情信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:remove')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/delApplyForDetails/{applydetailsIds}")
    public AjaxResult removeDetails(@PathVariable Long[] applydetailsIds) {
        return toAjax(glassesApplyForService.removeDetailsByIds(Arrays.asList(applydetailsIds)));
    }

    /**
     * 获取库存申请详情信息详细信息
     */
    @ApiOperation(value = "获取库存申请详情信息详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:query')")
    @GetMapping(value = "/getApplyForDetails/{applydetailsId}")
    public AjaxResult getApplyForDetailsById(@PathVariable("applydetailsId") Long id) {
        return success(glassesApplyForService.getApplyForDetailsById(id));
    }

    /**
     * 修改库存申请详情信息
     */
    @ApiOperation(value = "修改库存申请详情信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:edit')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/updateApplyForDetails")
    public AjaxResult updateApplyForDetails(@RequestBody GlassesApplyForDetails applyForDetails) {
        if (applyForDetails.getNumber() > glassesInventoryService.getInventoryById(applyForDetails.getGinventoryId()).getNumber()) {
            return error("失败，商品‘" + glassesInventoryService.getInventoryById(applyForDetails.getGinventoryId()).getGgoodsId() + "’库存不足");
        }
        return toAjax(glassesApplyForService.updateApplyForDetailsById(applyForDetails));
    }

    /**
     * 审批库存申请信息
     */
    @ApiOperation(value = "审批库存申请信息")
    @PreAuthorize("@ss.hasPermi('glasses:applyFor:examine')")
    @Log(title = "库存申请信息管理", businessType = BusinessType.UPDATE)
    @GetMapping("/examineAndApprove/{applyforId}")
    public AjaxResult examineAndApprove(@PathVariable("applyforId") Long applyforId) {
        List<GlassesApplyForDetails> applyForDetailsList = glassesApplyForService.getApplyForDetailsByApplyForId(applyforId);
        for (GlassesApplyForDetails applyForDetails : applyForDetailsList) {
            if (applyForDetails.getNumber() > glassesInventoryService.getInventoryById(applyForDetails.getGinventoryId()).getNumber()) {
                return error("失败，商品‘" + glassesInventoryService.getInventoryById(applyForDetails.getGinventoryId()).getGgoodsId() + "’库存不足");
            }
        }
        return toAjax(glassesApplyForService.updateInventoryById(applyforId));
    }
}
