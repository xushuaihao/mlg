package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 品牌类型Mapper接口
 *
 * @author xshao
 * @date 2023-08-18
 */
public interface GlassesBrandMapper extends BaseMapper<GlassesBrand> {

    public List<GlassesBrand> selectPageList(@Param("glassesBrand") GlassesBrand glassesBrand);

    public int updateBrand(GlassesBrand brand);

    /**
     * 检查品牌名称唯一性
     * @param gbrandName
     * @return
     */
    public GlassesBrand checkBrandNameUnique(String gbrandName);
}
