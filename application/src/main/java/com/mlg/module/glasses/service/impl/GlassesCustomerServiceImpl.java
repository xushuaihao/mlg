package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesCustomerMapper;
import com.mlg.module.glasses.domain.GlassesCustomer;
import com.mlg.module.glasses.service.IGlassesCustomerService;

import java.util.List;

/**
 * 客户信息管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesCustomerServiceImpl extends ServiceImpl<GlassesCustomerMapper, GlassesCustomer> implements IGlassesCustomerService {

    @Override
    public List<GlassesCustomer> selectCustomerList(GlassesCustomer glassesCustomer) {
        return getBaseMapper().selectPageList(glassesCustomer);
    }
}