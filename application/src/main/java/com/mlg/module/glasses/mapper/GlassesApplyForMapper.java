package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesApplyFor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mlg.module.glasses.domain.GlassesApplyForDetails;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.domain.GlassesInventory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存申请信息管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesApplyForMapper extends BaseMapper<GlassesApplyFor> {

    /**
     * 查询库存申请列表
     * @param applyFor
     * @return
     */
    public List<GlassesApplyFor> selectApplyForList(@Param("applyFor") GlassesApplyFor applyFor);

    /**
     * 根据库存申请Id查询库存申请信息
     * @param applyForId
     * @return
     */
    public GlassesApplyFor selectApplyForById(Long applyForId);

    /**
     * 查询库存申请详情列表
     * @param applyForDetails
     * @return
     */
    public List<GlassesApplyForDetails> selectApplyForDetailsList(@Param("applyForDetails") GlassesApplyForDetails applyForDetails);

    /**
     * 检查库存申请详情里库存Id的唯一性
     * @param applyForDetails
     * @return
     */
    public GlassesApplyForDetails checkGinventoryIdUnique(@Param("applyForDetails") GlassesApplyForDetails applyForDetails);

    /**
     * 增加库存申请详情中商品的数量
     * @param details
     * @return
     */
    public boolean addDetails(GlassesApplyForDetails details);

    /**
     * 向库存申请详情中添加库存商品
     * @param details
     * @return
     */
    public boolean addGoodsToDetails(GlassesApplyForDetails details);

    /**
     * 根据Id删除库存申请详情
     * @param applydetailsId
     * @return
     */
    public boolean deleteDetailsById(Long applydetailsId);

    /**
     * 根据Id查询库存申请详情详细信息
     * @param applydetailsId
     * @return
     */
    public GlassesApplyForDetails selectApplyForDetailsById(Long applydetailsId);

    /**
     * 根据Id更新库存申请详情信息
     * @param applyForDetails
     * @return
     */
    public boolean updateApplyForDetailsById(@Param("applyForDetails") GlassesApplyForDetails applyForDetails);

    /**
     * 根据申请单号获取库存申请详情信息
     * @param applyforId
     * @return
     */
    public List<GlassesApplyForDetails> selectApplyForDetailsByApplyForId(Long applyforId);

    public boolean updateApplyForById(@Param("applyFor") GlassesApplyFor applyFor);

    public boolean deleteApplyForById(Long applyforId);
}
