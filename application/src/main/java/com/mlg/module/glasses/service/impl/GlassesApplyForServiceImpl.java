package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mlg.module.glasses.domain.GlassesApplyForDetails;
import com.mlg.module.glasses.domain.GlassesInventory;
import com.mlg.module.glasses.mapper.GlassesInventoryMapper;
import com.mlg.support.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesApplyForMapper;
import com.mlg.module.glasses.domain.GlassesApplyFor;
import com.mlg.module.glasses.service.IGlassesApplyForService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 库存申请信息管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesApplyForServiceImpl extends ServiceImpl<GlassesApplyForMapper, GlassesApplyFor> implements IGlassesApplyForService {

    @Autowired
    private GlassesApplyForMapper applyForMapper;
    @Autowired
    private GlassesInventoryMapper inventoryMapper;

    @Override
    public List<GlassesApplyFor> selectApplyForList(GlassesApplyFor applyFor) {
        return getBaseMapper().selectApplyForList(applyFor);
    }

    @Override
    public List<GlassesApplyForDetails> selectApplyForDetailsList(GlassesApplyForDetails applyForDetails) {
        return getBaseMapper().selectApplyForDetailsList(applyForDetails);
    }

    @Override
    @Transactional
    public Boolean addGoodsToDetails(List<GlassesApplyForDetails> applyForDetailsList) {
        int count = 0;
        for (GlassesApplyForDetails applyForDetails : applyForDetailsList) {
            GlassesApplyForDetails details = applyForMapper.checkGinventoryIdUnique(applyForDetails);
            if (StringUtils.isNotNull(details)) {
                applyForMapper.addDetails(applyForDetails);
            } else {
                applyForMapper.addGoodsToDetails(applyForDetails);
            }
            count += 1;
        }
        return applyForDetailsList.size() == count;
    }

    @Override
    public boolean removeDetailsByIds(List<Long> applydetailsIds) {
        int count = 0;
        for (Long applydetailsId : applydetailsIds) {
            if (applyForMapper.deleteDetailsById(applydetailsId)) {
                count += 1;
            }
        }
        return applydetailsIds.size() == count;
    }

    @Override
    public GlassesApplyForDetails getApplyForDetailsById(Long id) {
        return applyForMapper.selectApplyForDetailsById(id);
    }

    @Override
    public boolean updateApplyForDetailsById(GlassesApplyForDetails applyForDetails) {
        return applyForMapper.updateApplyForDetailsById(applyForDetails);
    }

    @Override
    public List<GlassesApplyForDetails> getApplyForDetailsByApplyForId(Long applyforId) {
        return applyForMapper.selectApplyForDetailsByApplyForId(applyforId);
    }

    @Override
    @Transactional
    public boolean updateInventoryById(Long applyforId) {
        int count = 0;
        List<GlassesApplyForDetails> applyForDetailsList = applyForMapper.selectApplyForDetailsByApplyForId(applyforId);
        for (GlassesApplyForDetails applyForDetails : applyForDetailsList) {
            GlassesInventory inventory = new GlassesInventory();
            inventory.setGgoodsId(inventoryMapper.selectInventoryById(applyForDetails.getGinventoryId()).getGgoodsId());
            inventory.setGstoreId(applyForMapper.selectApplyForById(applyForDetails.getApplyforId()).getGstoreId());
            inventory.setNumber(applyForDetails.getNumber());
            boolean success = false;
            System.out.println(inventoryMapper.selectInventoryByStoreAndGoods(inventory));
            if (null == inventoryMapper.selectInventoryByStoreAndGoods(inventory)) {
                System.out.println("1111111111111111111111");
                success = inventoryMapper.addInventoryByStore(inventory);
            } else {
                inventory.setGinventoryId(inventoryMapper.selectInventoryByStoreAndGoods(inventory).getGinventoryId());
                success = inventoryMapper.updateInventoryNumber(inventory);
            }
            if (inventoryMapper.minusInventoryByStore(inventory) && success) {
                count += 1;
            }
        }
        if (applyForDetailsList.size() == count) {
            GlassesApplyFor applyFor = applyForMapper.selectApplyForById(applyforId);
            applyFor.setStatus("2");
            applyForMapper.updateApplyForById(applyFor);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public GlassesApplyFor getApplyForById(Long id) {
        return getBaseMapper().selectApplyForById(id);
    }

    @Override
    public boolean removeApplyForById(Long applyforId) {
        return getBaseMapper().deleteApplyForById(applyforId);
    }
}