package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.mlg.support.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;

import java.util.List;


/**
 * 门店管理对象 glasses_store
 *
 * @author xshao
 * @date 2023-08-23
 */
@Data
@Accessors(chain = true)
@TableName("glasses_store")
@ApiModel(value = "GlassesStore对象", description = "门店管理")
public class GlassesStore {

    private static final long serialVersionUID = 1L;

    /**
     * 眼镜店Id
     */
    @ApiModelProperty("眼镜店Id")
    @TableId(value = "gstore_id", type = IdType.AUTO)
    private Integer gstoreId;

    /**
     * 眼镜店名称
     */
    @Excel(name = "眼镜店名称")
    @ApiModelProperty("眼镜店名称")
    @TableField("gstore_name")
    private String gstoreName;

    /**
     * 眼镜店编号
     */
    @Excel(name = "眼镜店编号")
    @ApiModelProperty("眼镜店编号")
    @TableField("gstore_code")
    private String gstoreCode;

    /**
     * 眼镜店店长Id
     */
    @Excel(name = "眼镜店店长Id")
    @ApiModelProperty("眼镜店店长Id")
    @TableField("user_id")
    private Long userId;

    /**
     * 眼镜店店长
     */
    @Excel(name = "眼镜店店长姓名")
    private String realName;

    /**
     * 所在地址
     */
    @Excel(name = "所在地址")
    @ApiModelProperty("所在地址")
    @TableField("address")
    private String address;

    /**
     * 眼镜店状态（0正常 1停用）
     */
    @Excel(name = "眼镜店状态", readConverterExp = "0=正常,1=停用")
    @ApiModelProperty("眼镜店状态（0正常 1停用）")
    @TableField("status")
    private String status;

    /**
     * 备注
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @ApiModelProperty("删除标志（0代表存在 1代表删除）")
    @TableField("is_delete")
    private Integer isDelete;

}
