package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单详情管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesIndentDetailsMapper extends BaseMapper<GlassesIndentDetails> {

    /**
     * 向订单详情中添加库存商品
     *
     * @param details
     * @return
     */
    public Boolean addGoodsToDetails(GlassesIndentDetails details);

    /**
     * 从库存中减去库存数量
     *
     * @param details
     * @return
     */
    public Boolean minusInventory(GlassesIndentDetails details);

    /**
     * 增加商品详情中商品的数量
     *
     * @param details
     * @return
     */
    public Boolean addDetails(GlassesIndentDetails details);

    /**
     * 查询商品详情列表
     *
     * @param details
     * @return
     */
    public List<GlassesIndentDetails> selectDetailsList(@Param("details") GlassesIndentDetails details);

    /**
     * 检查订单详情里库存Id的唯一性
     *
     * @param details
     * @return
     */
    public GlassesIndentDetails checkGinventoryIdUnique(@Param("details") GlassesIndentDetails details);

    /**
     * 通过订单详情Id删除订单详情
     *
     * @param gdetailsId
     * @return
     */
    public Boolean deleteDetailsById(Long gdetailsId);


    /**
     * 通过订单详情Id查询订单详情信息
     *
     * @param gdetailsId
     * @return
     */
    public GlassesIndentDetails selectById(Long gdetailsId);

    /**
     * 通过订单Id查询订单详情信息列表
     *
     * @param gdetailsId
     * @return
     */
    public List<GlassesIndentDetails> selectByGindentId(Long gdetailsId);
}
