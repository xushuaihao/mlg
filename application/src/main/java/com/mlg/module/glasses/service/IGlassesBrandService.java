package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesBrand;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 品牌类型Service接口
 *
 * @author xshao
 * @date 2023-08-18
 */
public interface IGlassesBrandService extends IService<GlassesBrand> {
    public List<GlassesBrand> selectBrandList(@Param("glassesBrand") GlassesBrand glassesBrand);

    public int updateBrandStatus(GlassesBrand brand);

    /**
     * 检验品牌名称是否唯一
     *
     * @param glassesBrand
     * @return
     */
    public String checkBrandNameUnique(GlassesBrand glassesBrand);
}