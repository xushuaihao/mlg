package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品信息管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesGoodsMapper extends BaseMapper<GlassesGoods> {

    public List<GlassesGoods> selectPageList(@Param("glassesGoods") GlassesGoods glassesGoods);

    public GlassesGoods selectGoodsById(Long id);
}
