package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mlg.support.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesIndentDetailsMapper;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.service.IGlassesIndentDetailsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 订单详情管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesIndentDetailsServiceImpl extends ServiceImpl<GlassesIndentDetailsMapper, GlassesIndentDetails> implements IGlassesIndentDetailsService {

    @Autowired
    private GlassesIndentDetailsMapper indentDetailsMapper;

    @Override
    @Transactional
    public Boolean addGoodsToDetails(List<GlassesIndentDetails> detailsList) {
        int count = 0;
        for (GlassesIndentDetails details : detailsList) {
            details.setNumber(1L);
            boolean success1 = false;
            GlassesIndentDetails indentDetails = indentDetailsMapper.checkGinventoryIdUnique(details);
            if (StringUtils.isNotNull(indentDetails) && indentDetails.getGinventoryId().longValue() == details.getGinventoryId().longValue()) {
                success1 = indentDetailsMapper.addDetails(details);
            } else {
                success1 = indentDetailsMapper.addGoodsToDetails(details);

            }
            boolean success2 = indentDetailsMapper.minusInventory(details);
            if (success1 && success2) {
                count += 1;
            }
        }
        return detailsList.size() == count;
    }

    @Override
    public List<GlassesIndentDetails> selectDetailsList(GlassesIndentDetails details) {
        return getBaseMapper().selectDetailsList(details);
    }

    @Override
    public Boolean removeDetailsByIds(Long[] gdetailsIds) {
        int count = 0;
        for (Long gdetailsId : gdetailsIds) {
            if (indentDetailsMapper.deleteDetailsById(gdetailsId)) {
                count += 1;
            }
        }
        return gdetailsIds.length == count;
    }

    @Override
    public List<GlassesIndentDetails> selectDetailsByGindentId(Long gindentId) {
        return getBaseMapper().selectByGindentId(gindentId);
    }
}