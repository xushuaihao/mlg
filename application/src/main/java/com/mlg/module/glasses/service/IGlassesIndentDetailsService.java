package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单详情管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesIndentDetailsService extends IService<GlassesIndentDetails> {

    public Boolean addGoodsToDetails(List<GlassesIndentDetails> detailsList);

    public List<GlassesIndentDetails> selectDetailsList(@Param("details") GlassesIndentDetails details);

    public Boolean removeDetailsByIds(Long[] gdetailsIds);

    public List<GlassesIndentDetails> selectDetailsByGindentId(Long gindentId);
}