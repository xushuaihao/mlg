package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesIndentMapper;
import com.mlg.module.glasses.domain.GlassesIndent;
import com.mlg.module.glasses.service.IGlassesIndentService;

import java.util.List;

/**
 * 订单信息管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesIndentServiceImpl extends ServiceImpl<GlassesIndentMapper, GlassesIndent> implements IGlassesIndentService {

    @Override
    public List<GlassesIndent> selectIndentList(GlassesIndent glassesIndent) {
        return getBaseMapper().selectPageList(glassesIndent);
    }

    @Override
    public GlassesIndent getIndentById(Long id) {
        return getBaseMapper().selectIndentById(id);
    }

    @Override
    public int updateIndent(GlassesIndent glassesIndent) {
        return getBaseMapper().updateIndent(glassesIndent);
    }

    @Override
    public boolean removeIndentById(Long gindentId) {
        return getBaseMapper().deleteIndentById(gindentId);
    }
}