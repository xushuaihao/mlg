package com.mlg.module.glasses.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mlg.module.glasses.domain.GlassesIndentDetails;
import com.mlg.module.glasses.service.IGlassesIndentDetailsService;
import com.mlg.module.glasses.service.IGlassesInventoryService;
import com.mlg.support.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesIndent;
import com.mlg.module.glasses.service.IGlassesIndentService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 订单信息管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "订单信息管理模块")
@RestController
@RequestMapping("/glasses/indent")
public class GlassesIndentController extends BaseController {

    @Autowired
    private IGlassesIndentService glassesIndentService;
    @Autowired
    private IGlassesIndentDetailsService glassesIndentDetailsService;
    @Autowired
    private IGlassesInventoryService glassesInventoryService;

    /**
     * 查询订单信息列表
     */
    @ApiOperation(value = "查询订单信息列表")
//    @PreAuthorize("@ss.hasPermi('glasses:indent:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesIndent glassesIndent) {
        startPage();
        List<GlassesIndent> indentList = glassesIndentService.selectIndentList(glassesIndent);
        return getDataTable(indentList);
    }

    /**
     * 导出订单信息列表
     */
    @ApiOperation(value = "导出订单信息列表")
    @PreAuthorize("@ss.hasPermi('glasses:indent:export')")
    @Log(title = "订单信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response) {
        LambdaQueryWrapper<GlassesIndent> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesIndent> list = glassesIndentService.list(queryWrapper);
        ExcelUtil<GlassesIndent> util = new ExcelUtil<>(GlassesIndent.class);
        util.exportExcel(response, list, "订单信息管理数据");
    }


    /**
     * 获取订单信息管理详细信息
     */
    @ApiOperation(value = "获取订单信息管理详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:indent:query')")
    @GetMapping(value = "/{gindentId}")
    public AjaxResult getInfo(@PathVariable("gindentId") Long id) {
        return success(glassesIndentService.getIndentById(id));
    }

    /**
     * 新增订单信息管理
     */
    @ApiOperation(value = "新增订单信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:indent:add')")
    @Log(title = "订单信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesIndent glassesIndent) {
        return toAjax(glassesIndentService.save(glassesIndent));
    }

    /**
     * 修改订单信息管理
     */
    @ApiOperation(value = "修改订单信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:indent:edit')")
    @Log(title = "订单信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesIndent glassesIndent) {
        return toAjax(glassesIndentService.updateIndent(glassesIndent));
    }

    /**
     * 删除订单信息管理
     */
    @ApiOperation(value = "删除订单信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:indent:remove')")
    @Log(title = "订单信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{gindentIds}")
    public AjaxResult remove(@PathVariable("gindentIds") Long[] gindentIds) {
        int count = 0;
        for (Long gindentId : gindentIds) {
            List<GlassesIndentDetails> detailsList = glassesIndentDetailsService.selectDetailsByGindentId(gindentId);
            if (detailsList.size() > 0) {
                Long[] gdetailsIds = new Long[detailsList.size()];
                int i = 0;
                for (GlassesIndentDetails details : detailsList) {
                    gdetailsIds[i] = details.getGdetailsId();
                    i++;
                }
                if (glassesInventoryService.updateInventoryNumber(gdetailsIds)) {
                    if (glassesIndentService.removeIndentById(gindentId))
                        count += 1;
                }
            } else {
                if (glassesIndentService.removeIndentById(gindentId))
                    count += 1;
            }
        }
        return toAjax(gindentIds.length == count);
    }


    /**
     * 修改订单状态管理
     */
    @ApiOperation(value = "修改订单状态管理")
    @PreAuthorize("@ss.hasPermi('glasses:indent:edit')")
    @Log(title = "订单信息管理", businessType = BusinessType.UPDATE)
    @PutMapping("/updateIndentStatus")
    public AjaxResult updateIndentStatus(@RequestBody HashMap data) {
        GlassesIndent indent = new GlassesIndent();
        int gindentId = (int) data.get("gindentId");
        indent.setGindentId((long) gindentId);
        if (data.get("type").equals("charge")) {
            indent.setStatus("2");
        } else if (data.get("type").equals("pickUp")) {
            indent.setStatus("3");
        } else if (data.get("type").equals("refund")) {
            List<GlassesIndentDetails> detailsList = glassesIndentDetailsService.selectDetailsByGindentId((long) gindentId);
            if (detailsList.size() > 0) {
                Long[] gdetailsIds = new Long[detailsList.size()];
                int i = 0;
                for (GlassesIndentDetails details : detailsList) {
                    gdetailsIds[i] = details.getGdetailsId();
                    i++;
                }
                glassesInventoryService.updateInventoryNumber(gdetailsIds);
            }
            indent.setStatus("5");
        }
        return toAjax(glassesIndentService.updateIndent(indent));
    }
}
