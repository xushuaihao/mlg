package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mlg.support.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesGoods;
import com.mlg.module.glasses.service.IGlassesGoodsService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 商品信息管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "商品信息管理模块")
@RestController
@RequestMapping("/glasses/goods")
public class GlassesGoodsController extends BaseController {

    @Autowired
    private IGlassesGoodsService glassesGoodsService;

    /**
     * 查询商品信息管理列表
     */
    @ApiOperation(value = "查询商品信息列表")
//    @PreAuthorize("@ss.hasPermi('glasses:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesGoods glassesGoods) {
        startPage();
        List<GlassesGoods> goodsList = glassesGoodsService.selectGoodsList(glassesGoods);
        return getDataTable(goodsList);
    }

    /**
     * 导出商品信息管理列表
     */
    @ApiOperation(value = "导出商品信息列表")
    @PreAuthorize("@ss.hasPermi('glasses:goods:export')")
    @Log(title = "商品信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response) {
        LambdaQueryWrapper<GlassesGoods> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesGoods> list = glassesGoodsService.list(queryWrapper);
        ExcelUtil<GlassesGoods> util = new ExcelUtil<GlassesGoods>(GlassesGoods.class);
        util.exportExcel(response, list, "商品信息管理数据");
    }


    /**
     * 获取商品信息详细信息
     */
    @ApiOperation(value = "获取商品详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:goods:query')")
    @GetMapping(value = "/{ggoodsId}")
    public AjaxResult getInfo(@PathVariable("ggoodsId") Long id) {
        return success(glassesGoodsService.getGoodsById(id));
    }

    /**
     * 新增商品信息
     */
    @ApiOperation(value = "新增商品信息")
    @PreAuthorize("@ss.hasPermi('glasses:goods:add')")
    @Log(title = "商品信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesGoods glassesGoods) {
        return toAjax(glassesGoodsService.save(glassesGoods));
    }

    /**
     * 修改商品信息
     */
    @ApiOperation(value = "修改商品信息")
    @PreAuthorize("@ss.hasPermi('glasses:goods:edit')")
    @Log(title = "商品信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesGoods glassesGoods) {
        return toAjax(glassesGoodsService.updateById(glassesGoods));
    }


    /**
     * 删除商品信息
     */
    @ApiOperation(value = "删除商品信息")
    @PreAuthorize("@ss.hasPermi('glasses:goods:remove')")
    @Log(title = "商品信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ggoodsIds}")
    public AjaxResult remove(@PathVariable Integer[] ggoodsIds) {
        return toAjax(glassesGoodsService.removeBatchByIds(Arrays.asList(ggoodsIds)));
    }
}
