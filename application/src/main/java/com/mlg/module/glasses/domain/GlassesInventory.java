package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.mlg.support.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;

import java.math.BigDecimal;


/**
 * 门店库存管理对象 glasses_inventory
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_inventory")
@ApiModel(value = "GlassesInventory对象", description = "门店库存管理")
public class GlassesInventory {

    private static final long serialVersionUID = 1L;

    /**
     * 库存Id
     */
    @ApiModelProperty("库存Id")
    @TableId(value = "ginventory_id", type = IdType.AUTO)
    private Long ginventoryId;

    /**
     * 商品Id
     */
    @Excel(name = "商品Id")
    @ApiModelProperty("商品Id")
    @TableField("ggoods_id")
    private Long ggoodsId;

    /**
     * 门店Id
     */
    @Excel(name = "门店Id")
    @ApiModelProperty("门店Id")
    @TableField("gstore_id")
    private Long gstoreId;

    /**
     * 库存数量
     */
    @Excel(name = "库存数量")
    @ApiModelProperty("库存数量")
    @TableField("number")
    private Long number;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String ggoodsName;

    /**
     * 商品类型
     */
    @Excel(name = "商品类型")
    private String ggoodsType;

    /**
     * 镜片度数
     */
    @Excel(name = "镜片度数")
    private String lensDegree;

    /**
     * 商品价格
     */
    @Excel(name = "商品价格")
    private BigDecimal ggoodsPrice;

    /**
     * 品牌Id
     */
    @Excel(name = "品牌Id")
    private Long gbrandId;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String gbrandName;

    /**
     * 眼镜店名称
     */
    @Excel(name = "眼镜店名称")
    private String gstoreName;

    /**
     * 眼镜店编号
     */
    @Excel(name = "眼镜店编号")
    private String gstoreCode;

    /**
     * 库存申请数量
     */
    @Excel(name = "库存申请数量")
    private Long applyForNum;

    /**
     * 库存状态（0正常 1停用）
     */
    @Excel(name = "库存状态", readConverterExp = "0=正常,1=停用")
    @ApiModelProperty("库存状态（0正常 1停用）")
    @TableField("status")
    private String status;

    /**
     * 删除标志
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @ApiModelProperty("删除标志（0代表存在 1代表删除）")
    @TableField("is_delete")
    private Integer isDelete;

}
