package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mlg.module.glasses.domain.GlassesStore;
import com.mlg.support.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesBrandMapper;
import com.mlg.module.glasses.domain.GlassesBrand;
import com.mlg.module.glasses.service.IGlassesBrandService;

import java.util.List;

/**
 * 品牌类型Service业务层处理
 *
 * @author xshao
 * @date 2023-08-18
 */
@Service
public class GlassesBrandServiceImpl extends ServiceImpl<GlassesBrandMapper, GlassesBrand> implements IGlassesBrandService {

    @Autowired
    private GlassesBrandMapper brandMapper;

    @Override
    public List<GlassesBrand> selectBrandList(GlassesBrand glassesBrand) {
        return getBaseMapper().selectPageList(glassesBrand);
    }

    @Override
    public int updateBrandStatus(GlassesBrand brand) {
        return brandMapper.updateBrand(brand);
    }

    @Override
    public String checkBrandNameUnique(GlassesBrand glassesBrand) {
        Long gBrandId = StringUtils.isNull(glassesBrand.getGbrandId()) ? -1L : glassesBrand.getGbrandId();
        GlassesBrand info = brandMapper.checkBrandNameUnique(glassesBrand.getGbrandName());
        if (StringUtils.isNotNull(info) && info.getGbrandId().longValue() != gBrandId.longValue()) {
            return "1";
        }
        return "0";
    }
}