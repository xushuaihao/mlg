package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mlg.module.glasses.service.IGlassesCustomerService;
import com.mlg.support.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesCustomer;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 客户信息管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "客户信息管理模块")
@RestController
@RequestMapping("/glasses/customer")
public class GlassesCustomerController extends BaseController {

    @Autowired
    private IGlassesCustomerService glassesCustomerService;

    /**
     * 查询客户信息管理列表
     */
    @ApiOperation(value = "查询客户信息管理列表")
//    @PreAuthorize("@ss.hasPermi('glasses:customer:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesCustomer glassesCustomer) {
        startPage();
        List<GlassesCustomer> customerList = glassesCustomerService.selectCustomerList(glassesCustomer);
        return getDataTable(customerList);
    }

    /**
     * 导出客户信息管理列表
     */
    @ApiOperation(value = "导出客户信息管理列表")
    @PreAuthorize("@ss.hasPermi('glasses:customer:export')")
    @Log(title = "客户信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesCustomer glassesCustomer) {
        LambdaQueryWrapper<GlassesCustomer> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesCustomer> list = glassesCustomerService.list(queryWrapper);
        ExcelUtil<GlassesCustomer> util = new ExcelUtil<GlassesCustomer>(GlassesCustomer.class);
        util.exportExcel(response, list, "客户信息管理数据");
    }


    /**
     * 获取客户信息管理详细信息
     */
    @ApiOperation(value = "获取客户信息管理详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:customer:query')")
    @GetMapping(value = "/{customerId}")
    public AjaxResult getInfo(@PathVariable("customerId") Long id) {
        return success(glassesCustomerService.getById(id));
    }

    /**
     * 新增客户信息管理
     */
    @ApiOperation(value = "新增客户信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:customer:add')")
    @Log(title = "客户信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesCustomer glassesCustomer) {
        return toAjax(glassesCustomerService.save(glassesCustomer));
    }

    /**
     * 修改客户信息管理
     */
    @ApiOperation(value = "修改客户信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:customer:edit')")
    @Log(title = "客户信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesCustomer glassesCustomer) {
        return toAjax(glassesCustomerService.updateById(glassesCustomer));
    }


    /**
     * 删除客户信息管理
     */
    @ApiOperation(value = "删除客户信息管理")
    @PreAuthorize("@ss.hasPermi('glasses:customer:remove')")
    @Log(title = "客户信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{customerIds}")
    public AjaxResult remove(@PathVariable Long[] customerIds) {
        return toAjax(glassesCustomerService.removeBatchByIds(Arrays.asList(customerIds)));
    }
}
