package com.mlg.module.glasses.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mlg.module.glasses.mapper.GlassesGoodsMapper;
import com.mlg.module.glasses.domain.GlassesGoods;
import com.mlg.module.glasses.service.IGlassesGoodsService;

import java.util.List;

/**
 * 商品信息管理Service业务层处理
 *
 * @author xshao
 * @date 2023-10-04
 */
@Service
public class GlassesGoodsServiceImpl extends ServiceImpl<GlassesGoodsMapper, GlassesGoods> implements IGlassesGoodsService {

    @Override
    public List<GlassesGoods> selectGoodsList(GlassesGoods glassesGoods) {
        return getBaseMapper().selectPageList(glassesGoods);
    }

    @Override
    public GlassesGoods getGoodsById(Long id) {
        return getBaseMapper().selectGoodsById(id);
    }
}