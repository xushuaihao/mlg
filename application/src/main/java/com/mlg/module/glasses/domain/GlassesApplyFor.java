package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;

import java.util.Date;


/**
 * 库存申请信息管理对象 glasses_apply_for
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_apply_for")
@ApiModel(value = "GlassesApplyFor对象", description = "库存申请信息管理")
public class GlassesApplyFor {

    private static final long serialVersionUID = 1L;

    /**
     * 申请Id
     */
    @ApiModelProperty("申请Id")
    @TableId(value = "applyfor_id", type = IdType.AUTO)
    private Long applyforId;

    /**
     * 门店Id
     */
    @Excel(name = "门店Id")
    @ApiModelProperty("门店Id")
    @TableField("gstore_id")
    private Long gstoreId;

    /**
     * 门店编码
     */
    @Excel(name = "门店编码")
    private String gstoreCode;

    /**
     * 状态（0 待处理，1 已处理）
     */
    @Excel(name = "状态", readConverterExp = "0=,待=处理，1,已=处理")
    @ApiModelProperty("状态（0 待处理，1 已处理）")
    @TableField("status")
    private String status;

    /**
     * 创建者
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 逻辑删除
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @JsonIgnore //发送转json时忽略
    @TableField("is_delete")
    private Integer isDelete;

}
