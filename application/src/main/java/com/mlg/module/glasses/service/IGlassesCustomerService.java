package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesCustomer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户信息管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesCustomerService extends IService<GlassesCustomer> {

    /**
     * 查询客户信息列表
     *
     * @param glassesCustomer
     * @return
     */
    public List<GlassesCustomer> selectCustomerList(@Param("glassesCustomer") GlassesCustomer glassesCustomer);
}