package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesInventory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 门店库存管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesInventoryService extends IService<GlassesInventory> {

    /**
     * 查询库存信息列表
     *
     * @param glassesInventory
     * @return
     */
    public List<GlassesInventory> selectInventoryList(@Param("glassesInventory") GlassesInventory glassesInventory);

    /**
     * 通过库存Id查询库存信息
     *
     * @param id
     * @return
     */
    public GlassesInventory getInventoryById(Long id);

    /**
     * 修改库存数量 只能增加
     *
     * @param gdetailsIds
     * @return
     */
    public Boolean updateInventoryNumber(Long[] gdetailsIds);

    public int updateByInventoryId(GlassesInventory glassesInventory);

    public String checkInventoryUnique(GlassesInventory glassesInventory);

    public boolean addInventoryByGgoodsIdStoreId(GlassesInventory glassesInventory);
}