package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mlg.module.glasses.domain.GlassesGoods;
import com.mlg.support.common.core.page.TableDataInfo;
import com.mlg.support.common.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesInventory;
import com.mlg.module.glasses.service.IGlassesInventoryService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 门店库存管理Controller
 *
 * @author xshao
 * @date 2023-10-04
 */
@Api(tags = "门店库存管理模块")
@RestController
@RequestMapping("/glasses/inventory")
public class GlassesInventoryController extends BaseController {

    @Autowired
    private IGlassesInventoryService glassesInventoryService;

    /**
     * 查询门店库存列表
     */
    @ApiOperation(value = "查询门店库存列表")
//    @PreAuthorize("@ss.hasPermi('glasses:inventory:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesInventory glassesInventory) {
        startPage();
        List<GlassesInventory> inventoryList = glassesInventoryService.selectInventoryList(glassesInventory);
        return getDataTable(inventoryList);
    }

    /**
     * 导出门店库存列表
     */
    @ApiOperation(value = "导出门店库存列表")
    @PreAuthorize("@ss.hasPermi('glasses:inventory:export')")
    @Log(title = "门店库存管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesInventory glassesInventory) {
        LambdaQueryWrapper<GlassesInventory> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesInventory> list = glassesInventoryService.list(queryWrapper);
        ExcelUtil<GlassesInventory> util = new ExcelUtil<GlassesInventory>(GlassesInventory.class);
        util.exportExcel(response, list, "门店库存数据");
    }


    /**
     * 获取门店库存详细信息
     */
    @ApiOperation(value = "获取门店库存详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:inventory:query')")
    @GetMapping(value = "/{ginventoryId}")
    public AjaxResult getInfo(@PathVariable("ginventoryId") Long id) {
        return success(glassesInventoryService.getInventoryById(id));
    }

    /**
     * 新增门店库存
     */
    @ApiOperation(value = "新增门店库存")
    @PreAuthorize("@ss.hasPermi('glasses:inventory:add')")
    @Log(title = "门店库存管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesInventory glassesInventory) {
        if (StringUtils.isNotNull(glassesInventory.getGgoodsId()) && "1".equals(glassesInventoryService.checkInventoryUnique(glassesInventory))) {
            return toAjax(glassesInventoryService.addInventoryByGgoodsIdStoreId(glassesInventory));
        }
        return toAjax(glassesInventoryService.save(glassesInventory));
    }

    /**
     * 修改门店库存
     */
    @ApiOperation(value = "修改门店库存")
    @PreAuthorize("@ss.hasPermi('glasses:inventory:edit')")
    @Log(title = "门店库存管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesInventory glassesInventory) {
        return toAjax(glassesInventoryService.updateByInventoryId(glassesInventory));
    }


    /**
     * 删除门店库存
     */
    @ApiOperation(value = "删除门店库存")
    @PreAuthorize("@ss.hasPermi('glasses:inventory:remove')")
    @Log(title = "门店库存管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ginventoryId}")
    public AjaxResult remove(@PathVariable Long[] ginventoryId) {
        return toAjax(glassesInventoryService.removeBatchByIds(Arrays.asList(ginventoryId)));
    }
}
