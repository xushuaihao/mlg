package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mlg.support.common.core.domain.entity.SysUser;
import com.mlg.support.common.core.page.TableDataInfo;
import com.mlg.support.common.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.models.auth.In;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesBrand;
import com.mlg.module.glasses.service.IGlassesBrandService;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 品牌类型Controller
 *
 * @author xshao
 * @date 2023-08-18
 */
@Api(tags = "品牌管理模块")
@RestController
@RequestMapping("/glasses/brand")
public class GlassesBrandController extends BaseController {

    @Autowired
    private IGlassesBrandService glassesBrandService;

    /**
     * 查询品牌列表
     */
    @ApiOperation(value = "查询品牌列表")
//    @PreAuthorize("@ss.hasPermi('glasses:brand:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesBrand glassesBrand) {
        startPage();
        List<GlassesBrand> brandList = glassesBrandService.selectBrandList(glassesBrand);
        return getDataTable(brandList);
    }

    /**
     * 导出品牌列表
     */
    @ApiOperation(value = "导出品牌列表")
    @PreAuthorize("@ss.hasPermi('glasses:brand:export')")
    @Log(title = "品牌类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesBrand glassesBrand) {
        LambdaQueryWrapper<GlassesBrand> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesBrand> list = glassesBrandService.list(queryWrapper);
        ExcelUtil<GlassesBrand> util = new ExcelUtil<GlassesBrand>(GlassesBrand.class);
        util.exportExcel(response, list, "品牌数据");
    }


    /**
     * 获取品牌详细信息
     */
    @ApiOperation(value = "获取品牌详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:brand:query')")
    @GetMapping(value = "/{gbrandId}")
    public AjaxResult getInfo(@PathVariable("gbrandId") Long id) {
        return success(glassesBrandService.getById(id));
    }

    /**
     * 新增品牌
     */
    @ApiOperation(value = "新增品牌")
    @PreAuthorize("@ss.hasPermi('glasses:brand:add')")
    @Log(title = "品牌管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GlassesBrand glassesBrand) {
        if (StringUtils.isNotEmpty(glassesBrand.getGbrandName()) && "1".equals(glassesBrandService.checkBrandNameUnique(glassesBrand))) {
            return error("新增品牌失败，品牌名称已存在");
        }
        return toAjax(glassesBrandService.save(glassesBrand));
    }

    /**
     * 修改品牌类型
     */
    @ApiOperation(value = "修改品牌")
    @PreAuthorize("@ss.hasPermi('glasses:brand:edit')")
    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesBrand glassesBrand) {
        if (StringUtils.isNotEmpty(glassesBrand.getGbrandName()) && "1".equals(glassesBrandService.checkBrandNameUnique(glassesBrand))) {
            return error("修改品牌'" + glassesBrand.getGbrandId() + "'失败，品牌名称已存在");
        }
        return toAjax(glassesBrandService.updateById(glassesBrand));
    }


    /**
     * 删除品牌
     */
    @ApiOperation(value = "删除品牌")
    @PreAuthorize("@ss.hasPermi('glasses:brand:remove')")
    @Log(title = "品牌管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{gBrandIds}")
    public AjaxResult remove(@PathVariable Integer[] gBrandIds) {
        return toAjax(glassesBrandService.removeBatchByIds(Arrays.asList(gBrandIds)));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('glasses:brand:edit')")
    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody GlassesBrand brand) {
        return toAjax(glassesBrandService.updateBrandStatus(brand));
    }
}
