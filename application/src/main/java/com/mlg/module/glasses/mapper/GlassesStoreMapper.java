package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 门店管理Mapper接口
 *
 * @author xshao
 * @date 2023-08-23
 */
public interface GlassesStoreMapper extends BaseMapper<GlassesStore> {

    public List<GlassesStore> selectPageList(@Param("glassesStore") GlassesStore glassesStore);

    public GlassesStore selectStoreById(Long id);

    public GlassesStore checkStoreCodeUnique(String gstoreCode);

    public GlassesStore checkUserIdUnique(Long userId);

    public int updateGstoreById(GlassesStore glassesStore);

    public GlassesStore selectStoreByUserId(Long userId);
}
