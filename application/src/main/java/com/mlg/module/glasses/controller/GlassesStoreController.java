package com.mlg.module.glasses.controller;

import java.util.List;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mlg.module.glasses.service.IGlassesStoreService;
import com.mlg.module.system.service.ISysUserService;
import com.mlg.support.common.core.domain.entity.SysUser;
import com.mlg.support.common.core.page.TableDataInfo;
import com.mlg.support.common.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mlg.support.common.annotation.Log;
import com.mlg.support.common.core.controller.BaseController;
import com.mlg.support.common.core.domain.AjaxResult;
import com.mlg.support.common.enums.BusinessType;
import com.mlg.module.glasses.domain.GlassesStore;
import com.mlg.support.common.utils.poi.ExcelUtil;

/**
 * 门店管理Controller
 *
 * @author xshao
 * @date 2023-08-23
 */
@Api(tags = "门店管理模块")
@RestController
@RequestMapping("/glasses/store")
public class GlassesStoreController extends BaseController {

    @Autowired
    private IGlassesStoreService glassesStoreService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询门店管理列表
     */
    @ApiOperation(value = "查询门店管理列表")
//    @PreAuthorize("@ss.hasPermi('glasses:store:list')")
    @GetMapping("/list")
    public TableDataInfo list(GlassesStore glassesStore) {
        startPage();
        List<GlassesStore> storeList = glassesStoreService.selectStoreList(glassesStore);
        return getDataTable(storeList);
    }

    /**
     * 导出门店管理列表
     */
    @ApiOperation(value = "导出门店管理列表")
    @PreAuthorize("@ss.hasPermi('glasses:store:export')")
    @Log(title = "门店管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GlassesStore glassesStore) {
        LambdaQueryWrapper<GlassesStore> queryWrapper = new LambdaQueryWrapper<>();
        List<GlassesStore> list = glassesStoreService.list(queryWrapper);
        ExcelUtil<GlassesStore> util = new ExcelUtil<GlassesStore>(GlassesStore.class);
        util.exportExcel(response, list, "门店管理数据");
    }


    /**
     * 获取门店管理详细信息
     */
    @ApiOperation(value = "获取门店管理详细信息")
    @PreAuthorize("@ss.hasPermi('glasses:store:query')")
    @GetMapping(value = "/{gstoreId}")
    public AjaxResult getInfo(@PathVariable("gstoreId") Long id) {
        return success(glassesStoreService.getStoreById(id));
    }

    /**
     * 新增门店管理
     */
    @ApiOperation(value = "新增门店管理")
    @PreAuthorize("@ss.hasPermi('glasses:store:add')")
    @Log(title = "门店管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody GlassesStore glassesStore) {
        if (StringUtils.isNotEmpty(glassesStore.getGstoreCode())
                && "1".equals(glassesStoreService.checkStoreCodeUnique(glassesStore))) {
            return error("新增门店'" + glassesStore.getGstoreCode() + "'失败，门店编码已存在");
        } else if (StringUtils.isNotNull(glassesStore.getUserId())
                && "1".equals(glassesStoreService.checkUserIdUnique(glassesStore))) {
            return error("新增门店'" + glassesStore.getGstoreCode() + "'失败，用户'" + userService.selectUserById(glassesStore.getUserId()).getRealName() + "'是其他门店的负责人");
        }

        if (StringUtils.isNotNull(glassesStore.getUserId()) && StringUtils.isNotEmpty(glassesStore.getGstoreCode())) {
            SysUser user = new SysUser();
            user.setUserId(glassesStore.getUserId());
            user.setGstoreCode(glassesStore.getGstoreCode());
            userService.updateUser(user);
        }

        return toAjax(glassesStoreService.save(glassesStore));
    }

    /**
     * 修改门店管理
     */
    @ApiOperation(value = "修改门店管理")
    @PreAuthorize("@ss.hasPermi('glasses:store:edit')")
    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GlassesStore glassesStore) {
        if (StringUtils.isNotEmpty(glassesStore.getGstoreCode())
                && "1".equals(glassesStoreService.checkStoreCodeUnique(glassesStore))) {
            return error("修改门店'" + glassesStore.getGstoreCode() + "'失败，门店编码已存在");
        } else if (StringUtils.isNotNull(glassesStore.getUserId())
                && "1".equals(glassesStoreService.checkUserIdUnique(glassesStore))) {
            return error("修改门店'" + glassesStore.getGstoreCode() + "'失败，用户'" + userService.selectUserById(glassesStore.getUserId()).getRealName() + "'是其他门店的负责人");
        }

        if (StringUtils.isNotNull(glassesStore.getUserId()) && StringUtils.isNotEmpty(glassesStore.getGstoreCode())) {
            SysUser user = new SysUser();
            user.setUserId(glassesStore.getUserId());
            user.setGstoreCode(glassesStore.getGstoreCode());
            userService.updateUser(user);
        }

        return toAjax(glassesStoreService.updateGstoreById(glassesStore));
    }

    /**
     * 删除门店管理
     */
    @ApiOperation(value = "删除门店管理")
    @PreAuthorize("@ss.hasPermi('glasses:store:remove')")
    @Log(title = "门店管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{gstoreIds}")
    public AjaxResult remove(@PathVariable Integer[] gstoreIds) {
        return toAjax(glassesStoreService.removeBatchByIds(Arrays.asList(gstoreIds)));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('glasses:store:edit')")
    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody GlassesStore store) {
        return toAjax(glassesStoreService.updateStoreStatus(store));
    }
}
