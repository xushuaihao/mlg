package com.mlg.module.glasses.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mlg.support.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 库存申请详情信息管理对象 glasses_apply_for_details
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_apply_for_details")
@ApiModel(value = "GlassesApplyForDetails对象", description = "库存申请详情信息管理")
public class GlassesApplyForDetails {

    private static final long serialVersionUID = 1L;

    /**
     * 申请详情Id
     */
    @ApiModelProperty("申请详情Id")
    @TableId(value = "applydetails_id", type = IdType.AUTO)
    private Long applydetailsId;

    /**
     * 申请Id
     */
    @Excel(name = "申请Id")
    @ApiModelProperty("申请Id")
    @TableField("applyfor_id")
    private Long applyforId;

    /**
     * 库存Id
     */
    @Excel(name = "库存Id")
    @ApiModelProperty("库存Id")
    @TableField("ginventory_id")
    private Long ginventoryId;

    /**
     * 申请数量
     */
    @Excel(name = "申请数量")
    @ApiModelProperty("申请数量")
    @TableField("number")
    private Long number;

    /**
     * 商品Id
     */
    @Excel(name = "商品Id")
    private Integer ggoodsId;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String ggoodsName;

    /**
     * 商品类型
     */
    @Excel(name = "商品类型")
    private String ggoodsType;

    /**
     * 镜片度数
     */
    @Excel(name = "镜片度数")
    private String lensDegree;

    /**
     * 商品价格
     */
    @Excel(name = "商品价格")
    private BigDecimal ggoodsPrice;

    /**
     * 品牌Id
     */
    @Excel(name = "品牌Id")
    private Integer gbrandId;

    /**
     * 逻辑删除
     */
    @TableLogic(value = "0", delval = "1") //逻辑删除
    @JsonIgnore //发送转json时忽略
    @TableField("is_delete")
    private Integer isDelete;
}
