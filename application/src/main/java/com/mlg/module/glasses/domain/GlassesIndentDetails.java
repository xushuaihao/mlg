package com.mlg.module.glasses.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mlg.support.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.mlg.support.common.annotation.Excel;


/**
 * 订单详情管理对象 glasses_indent_details
 *
 * @author xshao
 * @date 2023-10-04
 */
@Data
@Accessors(chain = true)
@TableName("glasses_indent_details")
@ApiModel(value = "GlassesIndentDetails对象", description = "订单详情管理")
public class GlassesIndentDetails {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value = "gdetails_id", type = IdType.AUTO)
    private Long gdetailsId;

    /**
     * 订单id
     */
    @Excel(name = "订单id")
    @ApiModelProperty("订单id")
    @TableField("gindent_id")
    private Long gindentId;

    /**
     * 库存商品Id
     */
    @Excel(name = "库存商品Id")
    @ApiModelProperty("库存商品Id")
    @TableField("ginventory_id")
    private Long ginventoryId;

    /**
     * 商品Id
     */
    @Excel(name = "商品Id")
    @ApiModelProperty("商品Id")
    @TableField("ggoods_id")
    private Long ggoodsId;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    @ApiModelProperty("商品名称")
    @TableField("ggoods_name")
    private String ggoodsName;

    /**
     * 商品价格
     */
    @Excel(name = "商品价格")
    @ApiModelProperty("商品价格")
    @TableField("ggoods_price")
    private BigDecimal ggoodsPrice;

    /**
     * 镜片度数
     */
    @Excel(name = "镜片度数")
    @ApiModelProperty("镜片度数")
    @TableField("lens_degree")
    private String lensDegree;

    /**
     * 商品数量
     */
    @Excel(name = "商品数量")
    @ApiModelProperty("商品数量")
    @TableField("number")
    private Long number;

    /**
     * 客户评价（0差，1良，2优）
     */
    @Excel(name = "客户评价", readConverterExp = "0=差，1良，2优")
    @ApiModelProperty("客户评价（0差，1良，2优）")
    @TableField("evaluate")
    private String evaluate;

    /**
     * 改进建议
     */
    @Excel(name = "改进建议")
    @ApiModelProperty("改进建议")
    @TableField("improvement")
    private String improvement;


}
