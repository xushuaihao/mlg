package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 门店管理Service接口
 *
 * @author xshao
 * @date 2023-08-23
 */
public interface IGlassesStoreService extends IService<GlassesStore> {

    /**
     * 查询门店信息列表
     *
     * @param glassesStore
     * @return
     */
    public List<GlassesStore> selectStoreList(@Param("glassesStore") GlassesStore glassesStore);

    /**
     * 通过门店Id获取门店信息
     *
     * @param id
     * @return
     */
    public GlassesStore getStoreById(Long id);

    /**
     * 修改门店状态
     *
     * @param store
     * @return
     */
    public int updateStoreStatus(GlassesStore store);

    /**
     * 检验门店编码是否唯一
     *
     * @param glassesStore
     * @return
     */
    public String checkStoreCodeUnique(GlassesStore glassesStore);

    /**
     * 检验该用户是否为其他门店负责人
     *
     * @param glassesStore
     * @return
     */
    public String checkUserIdUnique(GlassesStore glassesStore);

    public int updateGstoreById(GlassesStore glassesStore);
}