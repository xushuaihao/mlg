package com.mlg.module.glasses.mapper;

import com.mlg.module.glasses.domain.GlassesIndent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单信息管理Mapper接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface GlassesIndentMapper extends BaseMapper<GlassesIndent> {

    public List<GlassesIndent> selectPageList(@Param("glassesIndent") GlassesIndent glassesIndent);

    public GlassesIndent selectIndentById(Long id);

    public int updateIndent(@Param("glassesIndent") GlassesIndent glassesIndent);

    public boolean deleteIndentById(Long gindentId);
}
