package com.mlg.module.glasses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mlg.module.glasses.domain.GlassesApplyFor;
import com.mlg.module.glasses.domain.GlassesApplyForDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存申请信息管理Service接口
 *
 * @author xshao
 * @date 2023-10-04
 */
public interface IGlassesApplyForService extends IService<GlassesApplyFor> {

    /**
     * 查询库存申请信息列表
     *
     * @param applyFor
     * @return
     */
    public List<GlassesApplyFor> selectApplyForList(@Param("applyFor") GlassesApplyFor applyFor);

    /**
     * 查询库存申请详情信息列表
     *
     * @param applyForDetails
     * @return
     */
    public List<GlassesApplyForDetails> selectApplyForDetailsList(@Param("applyForDetails") GlassesApplyForDetails applyForDetails);

    public Boolean addGoodsToDetails(List<GlassesApplyForDetails> applyForDetailsList);

    public boolean removeDetailsByIds(List<Long> applydetailsIds);

    public GlassesApplyForDetails getApplyForDetailsById(Long id);

    public boolean updateApplyForDetailsById(@Param("applyForDetails") GlassesApplyForDetails applyForDetails);

    /**
     * 通过申请单号获取库存申请详情信息
     *
     * @param applyforId
     * @return
     */
    public List<GlassesApplyForDetails> getApplyForDetailsByApplyForId(Long applyforId);

    /**
     * 根据库存申请信息更新库存
     *
     * @param applyforId
     * @return
     */
    public boolean updateInventoryById(Long applyforId);

    /**
     * 根据申请Id查询库存申请信息
     *
     * @param id
     * @return
     */
    public GlassesApplyFor getApplyForById(Long id);

    public boolean removeApplyForById(Long applyforId);
}