import request from '@/utils/request'

// 查询门店库存管理列表
export function listInventory(query) {
    return request({
        url: '/glasses/inventory/list',
        method: 'get',
        params: query
    })
}

// 查询门店库存管理详细
export function getInventory(ggoodsId) {
    return request({
        url: '/glasses/inventory/' + ggoodsId,
        method: 'get'
    })
}

// 新增门店库存管理
export function addInventory(data) {
    return request({
        url: '/glasses/inventory',
        method: 'post',
        data: data
    })
}

// 修改门店库存管理
export function updateInventory(data) {
    return request({
        url: '/glasses/inventory',
        method: 'put',
        data: data
    })
}

// 删除门店库存管理
export function delInventory(ggoodsId) {
    return request({
        url: '/glasses/inventory/' + ggoodsId,
        method: 'delete'
    })
}
