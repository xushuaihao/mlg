import request from '@/utils/request'

// 查询订单信息管理列表
export function listIndent(query) {
    return request({
        url: '/glasses/indent/list',
        method: 'get',
        params: query
    })
}

// 查询订单信息管理详细
export function getIndent(gindentId) {
    return request({
        url: '/glasses/indent/' + gindentId,
        method: 'get'
    })
}

// 新增订单信息管理
export function addIndent(data) {
    return request({
        url: '/glasses/indent',
        method: 'post',
        data: data
    })
}

// 修改订单信息管理
export function updateIndent(data) {
    return request({
        url: '/glasses/indent',
        method: 'put',
        data: data
    })
}

// 删除订单信息管理
export function delIndent(gindentIds) {
    return request({
        url: '/glasses/indent/' + gindentIds,
        method: 'delete'
    })
}

//修改订单状态
export function updateIndentStatus(gindentId,type) {
    const data = {
        gindentId,
        type
    }
    return request({
        url: '/glasses/indent/updateIndentStatus',
        method: 'put',
        data: data
    })
}
