import request from '@/utils/request'

// 查询订单详情管理列表
export function listDetails(query) {
    return request({
        url: '/glasses/details/list',
        method: 'get',
        params: query
    })
}

// 查询订单详情管理详细
export function getDetails(gdetailsId) {
    return request({
        url: '/glasses/details/' + gdetailsId,
        method: 'get'
    })
}

// 新增订单详情管理
export function addDetails(data) {
    return request({
        url: '/glasses/details',
        method: 'post',
        data: data
    })
}

// 修改订单详情管理
export function updateDetails(data) {
    return request({
        url: '/glasses/details',
        method: 'put',
        data: data
    })
}

// 删除订单详情管理
export function delDetails(gdetailsIds) {
    return request({
        url: '/glasses/details/' + gdetailsIds,
        method: 'delete'
    })
}

// 订单详情添加库存商品
export function addGoodsToDetails(data) {
    return request({
        url: '/glasses/details/addGoodsToDetails',
        method: 'post',
        data: data
    })
}

export function minusNumber() {
    return request({
        url: '/glasses/details/minusNumber',
        method: 'post',
        data: data
    })
}
