import request from '@/utils/request'

// 查询门店管理列表
export function listStore(query) {
    return request({
        url: '/glasses/store/list',
        method: 'get',
        params: query
    })
}

// 查询门店管理详细
export function getStore(gstoreId) {
    return request({
        url: '/glasses/store/' + gstoreId,
        method: 'get'
    })
}

// 新增门店管理
export function addStore(data) {
    return request({
        url: '/glasses/store',
        method: 'post',
        data: data
    })
}

// 修改门店管理
export function updateStore(data) {
    return request({
        url: '/glasses/store',
        method: 'put',
        data: data
    })
}

// 删除门店管理
export function delStore(gstoreId) {
    return request({
        url: '/glasses/store/' + gstoreId,
        method: 'delete'
    })
}

// 门店状态修改
export function changeStoreStatus(gstoreId, status) {
    const data = {
        gstoreId,
        status
    }
    return request({
        url: '/glasses/store/changeStatus',
        method: 'put',
        data: data
    })
}
