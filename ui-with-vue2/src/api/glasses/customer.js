import request from '@/utils/request'

// 查询客户信息管理列表
export function listCustomer(query) {
    return request({
        url: '/glasses/customer/list',
        method: 'get',
        params: query
    })
}

// 查询客户信息管理详细
export function getCustomer(customerId) {
    return request({
        url: '/glasses/customer/' + customerId,
        method: 'get'
    })
}

// 新增客户信息管理
export function addCustomer(data) {
    return request({
        url: '/glasses/customer',
        method: 'post',
        data: data
    })
}

// 修改客户信息管理
export function updateCustomer(data) {
    return request({
        url: '/glasses/customer',
        method: 'put',
        data: data
    })
}

// 删除客户信息管理
export function delCustomer(customerId) {
    return request({
        url: '/glasses/customer/' + customerId,
        method: 'delete'
    })
}
