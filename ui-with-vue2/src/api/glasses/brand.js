import request from '@/utils/request'

// 查询品牌类型列表
export function listBrand(query) {
    return request({
        url: '/glasses/brand/list',
        method: 'get',
        params: query
    })
}

// 查询品牌类型详细
export function getBrand(gbrandId) {
    return request({
        url: '/glasses/brand/' + gbrandId,
        method: 'get'
    })
}

// 新增品牌类型
export function addBrand(data) {
    return request({
        url: '/glasses/brand',
        method: 'post',
        data: data
    })
}

// 修改品牌类型
export function updateBrand(data) {
    return request({
        url: '/glasses/brand',
        method: 'put',
        data: data
    })
}

// 删除品牌类型
export function delBrand(gBrandId) {
    return request({
        url: '/glasses/brand/' + gBrandId,
        method: 'delete'
    })
}

// 品牌状态修改
export function changeBrandStatus(gbrandId, status) {
    const data = {
        gbrandId,
        status
    }
    return request({
        url: '/glasses/brand/changeStatus',
        method: 'put',
        data: data
    })
}
