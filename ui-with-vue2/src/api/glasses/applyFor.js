import request from '@/utils/request'

// 查询库存申请信息管理列表
export function listApplyFor(query) {
    return request({
        url: '/glasses/applyFor/list',
        method: 'get',
        params: query
    })
}

// 查询库存申请详情信息管理列表
export function listApplyForDetails(query) {
    return request({
        url: '/glasses/applyFor/detailsList',
        method: 'get',
        params: query
    })
}

// 查询库存申请信息管理详细
export function getApplyFor(applyforId) {
    return request({
        url: '/glasses/applyFor/' + applyforId,
        method: 'get'
    })
}

// 新增库存申请信息管理
export function addApplyFor(data) {
    return request({
        url: '/glasses/applyFor',
        method: 'post',
        data: data
    })
}

// 修改库存申请信息管理
export function updateApplyFor(data) {
    return request({
        url: '/glasses/applyFor',
        method: 'put',
        data: data
    })
}

// 删除库存申请信息管理
export function delApplyFor(applyforId) {
    return request({
        url: '/glasses/applyFor/' + applyforId,
        method: 'delete'
    })
}

// 库存申请详情添加库存商品
export function addGoodsToDetails(data) {
    return request({
        url: '/glasses/applyFor/addGoodsToDetails',
        method: 'post',
        data: data
    })
}

// 删除库存详情信息管理
export function delApplyForDetails(applydetailsIds) {
    return request({
        url: '/glasses/applyFor/delApplyForDetails/' + applydetailsIds,
        method: 'delete'
    })
}

// 查询库存申请详情信息管理详细
export function getApplyForDetails(applydetailsId) {
    return request({
        url: '/glasses/applyFor/getApplyForDetails/' + applydetailsId,
        method: 'get'
    })
}

// 修改库存申请详情信息管理
export function updateApplyForDetails(data) {
    return request({
        url: '/glasses/applyFor/updateApplyForDetails',
        method: 'put',
        data: data
    })
}

// 审批库存申请信息管理
export function examineAndApprove(applyforId) {
    return request({
        url: '/glasses/applyFor/examineAndApprove/' + applyforId,
        method: 'get'
    })
}
